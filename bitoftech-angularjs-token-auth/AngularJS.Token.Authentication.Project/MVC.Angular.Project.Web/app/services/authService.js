﻿(function () {
    'use strict';

    angular.module('app')
        .factory('authService', authService);

    function authService($http, $q, localStorageService) {
        var serviceBase = 'http://localhost:52935/';        

        var _authentication = {
            isAuth: false,
            userName: ''
        };

        return {
            saveRegistration: saveRegistration,
            login: login,
            logOut: logOut,
            fillAuthData: fillAuthData,
            authentication: authentication
        };

        function saveRegistration(registration)
        {
            _logOut();

            return $http.post(serviceBase + 'api/account/register', register).then(function (respones) {
                return response;
            });
        }

        function login(loginData) {
            var data = "grant_type=password&username=" + loginData.userName + "&password=" + loginData.password;
            var deferred = $q.defer();

            $http.post(serviceBase + 'token', data, { headers: { 'Content-Type': 'application/x-www-form-urlencoded' } }).success(function (response) {

                localStorageService.set('authorizationData', { token: response.access_token, userName: loginData.userName });

                _authentication.isAuth = true;
                _authentication.userName = loginData.userName;

                deferred.resolve(response);

            }).error(function (err, status) {
                logOut();
                deferred.reject(err);
            });

            return deferred.promise;

        };       

        function logOut() {

            localStorageService.remove('authorizationData');

            _authentication.isAuth = false;
            _authentication.userName = "";

        };       
        
        function fillAuthData() {

            var authData = localStorageService.get('authorizationData');
            if (authData) {
                _authentication.isAuth = true;
                _authentication.userName = authData.userName;
            }

        }

        function authentication() {

        }
    }
})();