﻿(function () {
    'use strict';

    angular.module('app')
        .controller('indexController', indexController);

    function indexController($scope, authService) {
        $scope.logOut = function () {
            authService.logOut();
            $location.path('/home');
        }

        $scope.authentication = authService.authentication;

    }
})();