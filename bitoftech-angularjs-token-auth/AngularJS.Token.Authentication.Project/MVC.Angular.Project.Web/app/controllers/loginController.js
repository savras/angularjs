﻿(function () {
    'use strict';

    angular.module('app')
        .controller('loginController', loginController);

    function loginController($scope, $location, authService) {
        $scope.loginData = {
            userName: "",
            password: ""
        };

        $scope.message = "";
        $scope.login = login;

        function login() {
            authService.login($scope.loginData).then(function (response) {
                $location.path('/orders');
            },
             function (err) {
                 $scope.message = err.error_description;
             });
        }
    }
})();