﻿(function () {
    'use strict';

    // Third party modules
    // Angular loading bar: http://chieffancypants.github.io/angular-loading-bar/
    // Angular local storage: https://github.com/grevory/angular-local-storage
    angular.module('app', ['ngRoute', 'LocalStorageModule', 'angular-loading-bar']);  
})();