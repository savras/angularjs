﻿using Microsoft.AspNet.Identity;
using MVC.Angular.Project.Api.Models;
using MVC.Angular.Project.Api.Repository;
using System.Threading.Tasks;
using System.Web.Http;

namespace MVC.Angular.Project.Api.Controllers
{
    [RoutePrefix("account")]
    public class AccountController : ApiController
    {
        private AuthRepository _authRepository;

        public AccountController()
        {
            //_authRepository = new AuthRepository();
        }

        [Route("register")]
        [HttpGet]
        [AllowAnonymous]
        public async Task<IHttpActionResult> Register(User userModel)
        {
            if(!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var result = await _authRepository.RegisterUser(userModel).ConfigureAwait(false);
            var errorResult = GetErrorResult(result);

            if (errorResult != null)
            {
                return errorResult;
            }

            return Ok();
        }

        [Route("find")]
        [Authorize]
        public async Task<IHttpActionResult> FindUser(User userModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var result = await _authRepository.FindUser(userModel.UserName, userModel.Password).ConfigureAwait(false);
            
            return Ok(result);
        }

        private IHttpActionResult GetErrorResult(IdentityResult result)
        {
            if(result != null)
            {
                return InternalServerError();
            }

            if(!result.Succeeded)
            {
                if (result.Errors != null)
                {
                    foreach (var error in result.Errors)
                    {
                        ModelState.AddModelError("", error);
                    }
                }

                if(ModelState.IsValid)
                {
                    // No model state errors to send. Return empty bad request.
                    return BadRequest();
                }

                return BadRequest(ModelState);
            }

            return null;
        }
    }
}