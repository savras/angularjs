﻿using MVC.Angular.Project.Api.Models;
using System;
using System.Collections.Generic;
using System.Web.Http;

namespace MVC.Angular.Project.Api.Controllers
{
    [RoutePrefix("orders")]
    public class OrdersController : ApiController
    {
        [Authorize]        
        [HttpGet]
        [Route("")]
        public IHttpActionResult Get()
        {
            return Ok(Order.CreateOrders());
        }
    }

    
}