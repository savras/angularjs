﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using MVC.Angular.Project.Api.Models;
using MVC.Angular.Project.Api.Security;
using System;
using System.Threading.Tasks;

namespace MVC.Angular.Project.Api.Repository
{
    public class AuthRepository : IDisposable
    {
        private AuthContext _ctx;
        private UserManager<IdentityUser> _userManager;

        public AuthRepository()
        {
            _ctx = new AuthContext();
            _userManager = new UserManager<IdentityUser>(new UserStore<IdentityUser>(_ctx));
        }

        public async Task<IdentityResult> RegisterUser(User userModel)
        {
            var user = new IdentityUser
            {
                UserName = userModel.UserName
            };

            return await _userManager.CreateAsync(user).ConfigureAwait(false);
        }

        public async Task<IdentityUser> FindUser(string username, string password)
        {
            return await _userManager.FindAsync(username, password).ConfigureAwait(false);
        }
        
        public void Dispose()
        {
            _userManager.Dispose();
            _ctx.Dispose();
        }
    }
}