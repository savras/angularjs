﻿using Microsoft.Owin;
using Microsoft.Owin.Security.OAuth;
using MVC.Angular.Project.Api.Security;
using Owin;
using System;
using System.Web.Http;
using System.Web.Http.Batch;

[assembly: OwinStartup(typeof(MVC.Angular.Project.Api.Startup))]
namespace MVC.Angular.Project.Api
{    
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            string uri = "http://localhost:8080/";


            ConfigureOAuth(app);
            HttpConfiguration config = new HttpConfiguration();

            // Http Request Batching
            config.Routes.MapHttpBatchRoute(
                routeName: "batch",
                routeTemplate: "api/batch",
                batchHandler: new DefaultHttpBatchHandler(GlobalConfiguration.DefaultServer));

            WebApiConfig.Register(config);
            app.UseCors(Microsoft.Owin.Cors.CorsOptions.AllowAll);
            app.UseWebApi(config);
            
        }

        private void ConfigureOAuth(IAppBuilder app)
        {
            var OAuthServerOptions = new OAuthAuthorizationServerOptions
            {
                AllowInsecureHttp = true,
                TokenEndpointPath = new PathString("/token"),   // The path for generating tokens will be as :”http://localhost:port/token”.
                AccessTokenExpireTimeSpan = TimeSpan.FromDays(1),   // Valid for 24 hours.
                Provider = new SimpleAuthorizationServerProvider()
            };

            // IAppBuilder extensions, add the following authentication middleware to the pipeline.
            app.UseOAuthAuthorizationServer(OAuthServerOptions);
            app.UseOAuthBearerAuthentication(new OAuthBearerAuthenticationOptions());
        }
    }
}