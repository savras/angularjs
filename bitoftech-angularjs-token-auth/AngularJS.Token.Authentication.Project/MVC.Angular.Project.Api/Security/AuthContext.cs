﻿using Microsoft.AspNet.Identity.EntityFramework;

namespace MVC.Angular.Project.Api.Security
{
    public class AuthContext : IdentityDbContext<IdentityUser>
    {
        public AuthContext() : base("AuthContext")
        {

        }
    }
}