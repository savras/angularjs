﻿(function () {
    'use strict'

    angular.module('app', ['ui.router']);


    var hub = $.connection.gameHub; // Create a proxy to signalr hub on web server. Has the same name as the GameHub class.

    hub.client.hello = function (message) {
        alert(message)
    };

    // Open connection to SignalR Hub
    //$.connection.hub.start();
    $.connection.hub.start().done = function () {   // Promise
        hub.server.hello(); // Function name in GameHub
    };

    //hub.server.subscribe($scope.customerId); // Subscribe to a group for the customer
    //hub.client.addItem = function (item) { // Item added by me or someone else, show it

    // unsubscribe to stop to get notifications for old customer
    //hub.server.unsubscribe($scope.customerIdSubscribed);

    // subscribe to start to get notifications for new customer
    //hub.server.subscribe($scope.customerId);

    // signalr client functions
    //hub.client.addItem = function (item) {
    //    $scope.complaints.push(item);
    //    $scope.$apply(); // this is outside of angularjs, so need to apply
    //}    
})();