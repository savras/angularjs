﻿using AngularJS.SignalR.Game.Memory.Memory.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AngularJS.SignalR.Game.Api.Domain.Models;
using AngularJS.SignalR.Game.Api.Domain.Models.Operations;

namespace AngularJS.SignalR.Game.Memory.Memory
{
    public class MemoryRepository : IMemoryRepository
    {
        public User GetAccount(GetUserOperation operation)
        {
            return new User 
            {
                Name = "Obi Wan-Kenobi", 
                Username = "Ben"
            };
        }
    }
}
