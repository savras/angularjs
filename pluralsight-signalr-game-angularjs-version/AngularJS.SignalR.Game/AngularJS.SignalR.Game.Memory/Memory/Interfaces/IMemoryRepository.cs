﻿using AngularJS.SignalR.Game.Api.Domain.Models;
using AngularJS.SignalR.Game.Api.Domain.Models.Operations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AngularJS.SignalR.Game.Memory.Memory.Interfaces
{
    public interface IMemoryRepository
    {
        User GetAccount(GetUserOperation operation);
    }
}
