﻿using AngularJS.SignalR.Game.Api.Domain.Models.Operations;
using AngularJS.SignalR.Game.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AngularJS.SignalR.Game.Data.Repositories.Interfaces
{
    public interface IAccountRepository
    {
        User GetAccount(GetUserOperation operation);
    }
}
