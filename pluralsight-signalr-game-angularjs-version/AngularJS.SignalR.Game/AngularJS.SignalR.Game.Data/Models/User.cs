﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AngularJS.SignalR.Game.Data.Models
{
    public class User
    {
        public string Name { get; set; }
        public string Username { get; set; }
    }
}
