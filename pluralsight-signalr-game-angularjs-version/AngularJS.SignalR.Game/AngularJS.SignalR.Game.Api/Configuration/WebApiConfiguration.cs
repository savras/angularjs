﻿using Newtonsoft.Json.Serialization;
using Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace AngularJS.SignalR.Game.Api.Configuration
{
    public class WebApiConfiguration
    {
        public static void ConfigureRegistrations(HttpConfiguration config)
        {
            config.MapHttpAttributeRoutes();
            config.RemoveXmlFormatter();

            // Send camel casing propertys back to client.
            var jsonFormatter = config.Formatters.JsonFormatter;
            jsonFormatter.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
        }
    }
}
