﻿/*
 * Tutorial: http://www.asp.net/aspnet/overview/owin-and-katana/getting-started-with-owin-and-katana
 * Requires the following NuGet packages :
 *  Owin.Host.SystemWeb
 *  ASP.NET Web Api OWIN 2.2
 *  Owin.Cors
 */
using AngularJS.SignalR.Game.Api;
using AngularJS.SignalR.Game.Api.Configuration;
using Microsoft.Owin;
using Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

[assembly: OwinStartup(typeof(Startup))]
namespace AngularJS.SignalR.Game.Api
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            HttpConfiguration config = new HttpConfiguration();

            WebApiConfiguration.ConfigureRegistrations(config);


            // Tutorial: Add the error page middleware to the pipeline. 
            // install-package Microsoft.Owin.Diagnostics –Pre
            //app.UseErrorPage();
            //
            app.Run(context =>
            {
                // New code: Throw an exception for this URI path.
            //    if (context.Request.Path == "/fail")
            //    {
            //        throw new Exception("Random exception");
            //    }
                context.Response.ContentType = "text/plain";
                return context.Response.WriteAsync("Web api initialized.");
            });

            // Any connection or hub wire up and configuration should go here
            app.MapSignalR();
            app.UseCors(Microsoft.Owin.Cors.CorsOptions.AllowAll);
            app.UseWebApi(config);
        }
    }
}
