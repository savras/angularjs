﻿using System;
using Microsoft.AspNet.SignalR;

namespace AngularJS.SignalR.Game.Api.Hub
{
    public class GameHub : Microsoft.AspNet.SignalR.Hub
    {
        public void Subscribe(string customerId)
        {
            Groups.Add(Context.ConnectionId, customerId);
        }

        public void Unsubscribe(string customerId)
        {
            Groups.Remove(Context.ConnectionId, customerId);
        }

        public void Hello()
        {
            Clients.All.hello(DateTime.Now.ToString("T"));
        }
    }
}