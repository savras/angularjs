﻿using AngularJS.SignalR.Game.Api.Domain.Models;
using AngularJS.SignalR.Game.Api.Domain.Models.Operations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace AngularJS.SignalR.Game.Api.Controllers
{
    [RoutePrefix("api/account")]
    public class AccountController : ApiController
    {

        [Route("getUser")]
        [HttpPost]
        public User GetUser([FromBody] GetUserOperation operation)
        {
            return new User();
        }

    }
}
