﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AngularJS.SignalR.Game.Api.Domain.Models.Operations
{
    public class GetUserOperation
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }
}
