﻿namespace Project.Client.Resources
{
    public class UserResource
    {
        string FirstName { get; set; }
        string LastName { get; set; }
        int Age { get; set; }
    }
}
