﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace Project.WebApi.Configuration
{
    /// <summary>
    /// The global settings for the API.
    /// </summary>
    public interface IApiSettings
    {
        /// <summary>
        /// Gets the data store type to use.
        /// </summary>
        DataStoreTypes DataStoreType { get; }

        /// <summary>
        /// Gets the URL of the API.
        /// </summary>
        Uri ApiUrl { get; }
    }

    /// <summary>
    /// The global settings for the API.
    /// </summary>
    public class ApiSettings : IApiSettings
    {
        /// <summary>
        /// Constructor.
        /// </summary>
        ApiSettings() { }

        /// <summary>
        /// Gets the data store type to use.
        /// </summary>
        public DataStoreTypes DataStoreType { get; private set; }

        /// <summary>
        /// Gets the URL of the API.
        /// </summary>
        public Uri ApiUrl { get; private set; }

        /// <summary>
        /// Returns the settings from configuration.
        /// </summary>
        /// <returns>The API settings.</returns>
        public static ApiSettings LoadFromConfig()
        {
            return new ApiSettings
            {
                ApiUrl = ConfigurationHelper.ReadUriFromConfig("Project.WebApi.Url"),
                DataStoreType = ConfigurationHelper.ReadEnumFromConfig<DataStoreTypes>("Project.WebApi.DataStoreType")
            };
        }
    }
}