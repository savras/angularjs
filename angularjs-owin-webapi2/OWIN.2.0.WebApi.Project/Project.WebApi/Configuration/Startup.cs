﻿using Autofac;
using Autofac.Integration.WebApi;
using Owin;
using Project.Oracle;
using Project.WebApi.Controllers;
using Rtio.Scheduling.MineTargets.Data.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace Project.WebApi.Configuration
{
    /// <summary>
    /// The API startup routine.
    /// </summary>
    public class Startup
    {
        readonly IApiSettings _settings;

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="settings">The api settings.</param>
        public Startup(IApiSettings settings)
        {
            _settings = settings;
        }

        /// <summary>
        /// Configures the API.
        /// </summary>
        /// <param name="app">The application builder.</param>
        public void Configuration(IAppBuilder app)
        {
            var config = new HttpConfiguration();
            config.EnableCors();
            config.MapHttpAttributeRoutes();
            config.RemoveXmlFormatter();

            var container = CreateContainer();
            config.DependencyResolver = new AutofacWebApiDependencyResolver(container);

            app.UseWebApi(config);
        }

        /// <summary>
        /// Creates the container.
        /// </summary>
        IContainer CreateContainer()
        {
            var builder = new ContainerBuilder();

            // Settings
            builder.Register(x => _settings)
                   .As<IApiSettings>()
                   .SingleInstance();

            // Startables
            builder.RegisterAssemblyTypes(typeof(Startup).Assembly)
                .Where(x => typeof(IStartable).IsAssignableFrom(x) && x.IsAbstract == false)
                .As<IStartable>()
                .SingleInstance();

            switch (_settings.DataStoreType)
            {
                case DataStoreTypes.Memory:
                    builder.RegisterModule(new MemoryDataModule());
                    break;

                case DataStoreTypes.Oracle:
                    builder.RegisterModule(new OracleDataModule());
                    break;

                default:
                    throw new InvalidOperationException(String.Format("Unknown data store type '{0}'.", _settings.DataStoreType.ToString()));
            }

            builder.RegisterApiControllers(typeof(UserController).Assembly);

            return builder.Build();
        }
    }

    //private void Configure(IAppBuilder app)
    //{
    //    var builder = new ContainerBuilder();

    //    var config = new HttpConfiguration();
    //    // OPTIONAL: Register the Autofac filter provider.
    //    builder.RegisterWebApiFilterProvider(config);

    //    // Register dependencies, then...
    //    var container = builder.Build();

    //    config.DependencyResolver = new AutofacWebApiDependencyResolver(container);

    //    // You can register controllers all at once using assembly scanning...
    //    builder.RegisterApiControllers(Assembly.GetExecutingAssembly());
    //}
}
