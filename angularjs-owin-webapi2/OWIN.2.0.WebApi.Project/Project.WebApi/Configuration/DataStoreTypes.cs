﻿namespace Project.WebApi.Configuration
{
    /// <summary>
    /// The available data store types.
    /// </summary>
    public enum DataStoreTypes
    {    
        /// <summary>
        /// The in memory repositories.
        /// </summary>
        Memory,

        /// <summary>
        /// The oracle repositories.
        /// </summary>
        Oracle
    }
}
