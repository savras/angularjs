﻿using Project.Domain.Models;
using System.Collections.Generic;

namespace Project.Data
{
    public interface IMemoryUserRepository
    {
        IEnumerable<User> GetAll();
    }
}
