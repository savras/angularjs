﻿namespace Project.Data
{
    /// <summary>
    /// Marker interface for memory repositories & DI registration.
    /// </summary>
    public interface IMemoryRepository { }
}
