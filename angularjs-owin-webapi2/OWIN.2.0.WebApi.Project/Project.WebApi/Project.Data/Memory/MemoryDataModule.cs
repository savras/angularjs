﻿using Autofac;
using Project.Data;

namespace Rtio.Scheduling.MineTargets.Data.Memory
{
    public class MemoryDataModule : Module
    {
        /// <summary>
        /// Registers dependencies with the container builder.
        /// </summary>
        /// <param name="builder">The container builder.</param>
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterAssemblyTypes(typeof(MemoryDataModule).Assembly)
                .Where(x => typeof(IMemoryRepository).IsAssignableFrom(x) && x.IsAbstract == false)
                .AsImplementedInterfaces()
                .SingleInstance();
        }
    }
}
