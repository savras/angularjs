﻿using Project.Domain.Models;
using System.Collections.Generic;
namespace Project.Data.Memory
{
    public class MemoryUserRepository : IMemoryUserRepository, IMemoryRepository
    {
        readonly List<User> _users;

        public MemoryUserRepository()
        {
            _users = new List<User>();
            _users.AddRange(CreateMockUsers());
        }

        public IEnumerable<User> GetAll()
        {
            return _users;
        }

        IEnumerable<User> CreateMockUsers()
        {
            return new List<User>
            {
                new User
                {
                    FirstName = "John",
                    LastName = "Rambo",
                    Age = 41
                },
                                new User
                {
                    FirstName = "James",
                    LastName = "Bond",
                    Age = 35
                },
            };

        }
    }
}
