﻿using Project.Domain.Models;
using System.Web.Http;
using Project.Data;
using System.Collections.Generic;
using System.Web.Http.Cors;

namespace Project.WebApi.Controllers
{
    [RoutePrefix("user")]
    [EnableCors("*", "*","*")]
    public class UserController : ApiController
    {
        readonly IMemoryUserRepository _memoryUserRepository;

        public UserController(IMemoryUserRepository memoryUserRepository)
        {
            _memoryUserRepository = memoryUserRepository;
        }

        [HttpGet]
        [Route("all")]
        public IEnumerable<User> GetAll()
        {
            return _memoryUserRepository.GetAll();
        }
    }
}
