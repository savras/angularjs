﻿using Project.WebApi.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Owin.Hosting;

namespace Project.WebApi
{
    class Program
    {
        static void Main()
        {
            var settings = ApiSettings.LoadFromConfig();
            var startup = new Startup(settings);

            using(WebApp.Start(settings.ApiUrl.ToString(), startup.Configuration))
            {
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("Web API running on {0}\nPress enter to exit.", settings.ApiUrl);

                string input;

                do
                {
                    input = (Console.ReadLine() ?? string.Empty).ToLowerInvariant();
                    if(input.Equals("cls", StringComparison.InvariantCulture) || input.Equals("clear", StringComparison.InvariantCulture))
                    {
                        Console.Clear();
                    }
                    else if (input.Equals("help", StringComparison.InvariantCulture))
                    {
                        Console.WriteLine("Type 'clear' or 'cls' to clear the screen. Press enter to exit.");
                    }
                } while(!input.Equals(string.Empty));
            }
        }            
    }
}