﻿using System.Web.Http;

namespace Project.WebApi
{
    /// <summary>
    /// Helper extensions for <see cref="HttpConfiguration"/>.
    /// </summary>
    public static class Extensions
    {
        /// <summary>
        /// Removes the XML media formatter.
        /// </summary>
        /// <param name="config">The HTTP configuration.</param>
        /// <returns>The configuration.</returns>
        public static HttpConfiguration RemoveXmlFormatter(this HttpConfiguration config)
        {
            config.Formatters.Remove(config.Formatters.XmlFormatter);
            return config;
        }
    }
}