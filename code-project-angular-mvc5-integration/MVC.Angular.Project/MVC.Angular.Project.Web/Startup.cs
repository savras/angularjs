﻿using Microsoft.Owin;
using Owin;

// Assembly level metadata specifying the Startup class to use.
[assembly: OwinStartupAttribute(typeof(MVC.Angular.Project.Web.Startup))]
namespace MVC.Angular.Project.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
