﻿(function () {
    'use strict';

    angular.module('app')
        .config(['$httpProvider', config]);

    function config($httpProvider) {
        $httpProvider.interceptors.push(['$q', '$location', function ($q, $location) {
            return {
                response: function (response) {
                    if (response.status === 401) {
                        console.log("Response 401");
                    }                   

                    return response || $q.when(response);
                },
                responseError: function (rejection) {
                    if (rejection.status === 401) {
                        console.log("Response Error 401", rejection);
                        $location.path('/login');   //.search('returnUrl', $location.path());
                    }
                    console.log('Intercepted response error 401');

                    return $q.reject(rejection);
                }
            }
        }]);
    };
})();