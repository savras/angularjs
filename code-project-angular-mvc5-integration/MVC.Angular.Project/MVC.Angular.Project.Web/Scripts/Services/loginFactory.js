﻿(function () {
    'use strict';

    angular.module('app')
        .factory('loginFactory', loginFactory);

    function loginFactory($resource) {
        var account = $resource('/Account/Login', {}, {
            post: {
                method: 'POST', isArray: true, params: {
                    email: '@emailAddress',
                    password: '@password',
                    rememberMe: '@rememberMe'
                }
            }
        });

        return {
            login: login
        };

        function login(data) {
            account.post(data).$promise;
        }
    }

    loginFactory.$inject = ['$resource'];
})();