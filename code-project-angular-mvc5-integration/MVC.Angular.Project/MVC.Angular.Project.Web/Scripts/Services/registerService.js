﻿(function () {
    'use strict';

    angular.module('app')
        .service('registerService', registerService);

    function registerService($resource) {
        var account = $resource('/Account/Register', {}, {
            post: {
                method: 'POST', isArray: true, params: {
                    email: '@emailAddress',
                    password: '@password',
                    rememberMe: '@rememberMe'
                }
            }
        });

        return {
            register: register
        };

        function register(data) {
            account.post(data).$promise;
        }
    }

    registerService.$inject = ['$resource'];
})();