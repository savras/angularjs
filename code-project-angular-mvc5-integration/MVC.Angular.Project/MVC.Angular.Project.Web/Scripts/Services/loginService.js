﻿// Test factory usage

(function () {
    'use strict';

    angular.module('app')
        .service('loginService', loginService);

    function loginService() {
        function printState()
        {
            return 'State has been printed.';
        }        
    }
})();