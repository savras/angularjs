﻿(function () {
    'use strict';

    angular.module('app')
        .controller('routeOneController', routeOneController);

    function routeOneController($rootScope) {
        var vm = this;
        vm.input = '';
        vm.myForm = {};
        vm.submit = submit;
        vm.resetForm = resetForm;

        $rootScope.$on('$locationChangeStart', confirmNavigation);

        function submit() {
            console.log('Submitting...');
            var message = 'Submitted.'
            if (vm.myForm.$error['required'].length >= 0)
            {
                message = 'Invalid input.';
            }
            console.log(message);            
        }

        function resetForm() {
            vm.myForm.$rollbackViewValue();
            vm.myForm.$setPristine();
        }

        // http://weblogs.asp.net/dwahlin/cancelling-route-navigation-in-angularjs-controllers
        // http://stackoverflow.com/questions/14809686/showing-alert-in-angularjs-when-user-leaves-a-page
        function confirmNavigation(event, newUrl) {
            var proceed = true;
            if (vm.myForm != null && vm.myForm.$dirty) {
                proceed = confirm('Are you sure you want to proceed to ' + newUrl + '?');
            }

            if (!proceed) {
                event.preventDefault();
            }
            else {
                //$location.path(newUrl); //Go to page they're interested in
            }
        }
    }

    routeOneController.$inject = ['$rootScope'];
})();