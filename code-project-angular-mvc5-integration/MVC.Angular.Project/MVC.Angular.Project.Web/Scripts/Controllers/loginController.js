﻿(function () {
    'use strict';

    angular.module('app')
        .controller('loginController', loginController);

    function loginController($scope, $routeParams,  loginFactory) {
        var vm = this;

        vm.loginForm = {
            emailAddress: '',
            password: '',
            rememberMe: false,
            returnUrl: $routeParams.returnUrl
        };

        vm.login = login;

        function login() {
            loginFactory.login(vm.loginForm);
        };
    };

    loginController.$inject = ['$scope', '$routeParams', 'loginFactory'];
})();