﻿(function () {
    'use strict';

    angular.module('app')
        .controller('registerController', registerController);

    function registerController(registerService) {
        var vm = this;

        vm.registerForm = {
            emailAddress: '',
            password: '',
            confirmPassword: '',
            registrationFailure: false
        };

        vm.register = register;

        function register() {
            registerService.register(vm.registerForm);
        }
    }

    registerController.$inject = ['registerService'];
})();