﻿(function () {
    'use strict';

    angular.module('app')
           .controller('indexController', indexController)

    function indexController($scope) {
        var vm = this;
        vm.my = { message: false };
        vm.models = {
            helloAngular: 'I work!'
        };
    }

    // The $inject property of every controller (and pretty much every other type of object in Angular) needs to be a string array equal 
    // to the controllers arguments, only as strings
    indexController.$inject = ['$scope'];
})();