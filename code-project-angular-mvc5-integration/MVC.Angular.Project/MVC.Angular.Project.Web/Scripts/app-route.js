﻿(function () {
    'use strict';

    angular.module('app')
       .config(['$routeProvider', config]);

    function config($routeProvider) {
        $routeProvider
            .when('/routeOne', {
                templateUrl: 'RoutesDemo/One',
                controller: 'routeOneController as vm'
            })
            .when('/routeTwo/:donuts', {
                templateUrl: function (params) { return '/RoutesDemo/Two?donuts=' + params.donuts; }
            })
            .when('/routeThree', {  // Marked as authorized in controller.
                templateUrl: 'RoutesDemo/Three',                
            })
            .when('/login', {
                templateUrl: 'Account/Login',
                controller: 'loginController as vm'
            })
            .when('/register', {
                templateUrl: 'Account/Register',
                controller: 'registerController as vm'
            })
            .otherwise({
                redirectTo: '/routeOne'
            });

        console.log("Routes configured");
    };    
})();