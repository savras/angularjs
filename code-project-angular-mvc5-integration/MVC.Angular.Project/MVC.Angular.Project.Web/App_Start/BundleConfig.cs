﻿using System.Web;
using System.Web.Optimization;

namespace MVC.Angular.Project.Web
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new StyleBundle("~/Content/css").Include("~/Content/site.css"));

            BundleTable.EnableOptimizations = true;

            
#if !DEBUG
            bundles.Add(new ScriptBundle("~/bundles/app")
                   .Include("~/Scripts/app.js")
                   .Include("~/Scripts/app-route.js")
                   .Include("~/Scripts/app-config.js")
                   .IncludeDirectory("~/Scripts/Services", "*.js")
                   .IncludeDirectory("~/Scripts/Controllers", "*.js"));
#endif
        }
    }
}
