(function(module){
	'use strict';
	
	module.controller('gallery', gallery);
	
	function gallery() {
		var vm = this;
		
		vm.test = 'test';
		vm.password = '';
        vm.grade = function() {
             var size = vm.password.length;
             if (size > 8) {
                 vm.strength = 'strong';
             } else if (size > 3) {
                 vm.strength = 'medium';
             } else {
                 vm.strength = 'weak';
             }
         }
	}
	
})(angular.module('app'));