describe('gallery controller', function() {
	var $controller;
	
	// Load the module.
	beforeEach(module('app'));

	
	beforeEach(inject(function(_$controller_) {
		// Injector unwraps underscores when matching names.
		$controller = _$controller_;
		
	}));
	
    describe('password test', function() {        
        var controller
        var $scope;
        
        beforeEach(function() {
            $scope = {};
            vm = $controller('gallery'); //, { $scope: $scope });
        });
        
        
        it('sets the strength to "strong" if the password length is > 8 chars', function() {
            vm.password = 'longerThanEightCharacters';
            vm.grade();
            expect(vm.strength).toEqual('strong');
        });
        
       it('sets the strength to "weak" if the password length <3 chars', function() {
           vm.password = 'a';
           vm.grade();
           expect(vm.strength).toEqual('weak');
        });
    });
});