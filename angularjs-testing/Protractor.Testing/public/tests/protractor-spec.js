describe('protractor state routing', function() {
    it('should get title', function() {
        browser.get('http://localhost:8001/index.html').then(function() {   // IIS
          return browser.getTitle();
        }).then(function(title) {
          expect(title).toEqual('Protractor tests');
        });
    });
    
    it('should navigate to gallery', function() {
        browser.get('http://localhost:8001/index.html#/gallery');        
        expect(element(by.css('.content-title > label')).getText()).toEqual('Gallery');                   
    });
});