exports.config = {
    directConnect: true,
    rootElement: 'body',
    specs: ['todo-spec.js'],
    allScriptsTimeout: 300000
};