(function () {
    'use strict';
    
    angular.module('app')
           .controller('index', index);
    
    function index($scope, $state) {
        var vm = this;
         
        //$rootScope.$state = $state
        vm.state = $state;
        vm.welcome = 'Welcome!';        
        vm.submit = submit;
        
        function submit() {
            vm.welcome = 'Submitted!';
        }
    }
})();