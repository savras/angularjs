(function () {
    'use strict';
    
    // Root can be found at localhost:portNo
    angular.module('app')
        .config(['$stateProvider', '$urlRouterProvider', '$locationProvider', configure]);
    
    function configure($stateProvider, $urlRouterProvider, $locationProvider) {
        $urlRouterProvider.otherwise('/');  // url of an existing state specified below.        
        
        $stateProvider
            .state('index', {
                url: '/',   // The url to match when user enters in the url bar.
                templateUrl: '/index.html',
                controller: 'index as vm'
            })
            .state('portfolio', {
                url: '/portfolio',
                templateUrl: '/public/portfolio/portfolio.html',
                controller: 'portfolio as vm'
            })
            .state('gallery', {
                url: '/gallery',
                templateUrl: '/public/gallery/gallery.html',
                controller: 'gallery as vm'
            });
        
        // use HTML5 API
        $locationProvider.html5Mode(true);
    };
})();