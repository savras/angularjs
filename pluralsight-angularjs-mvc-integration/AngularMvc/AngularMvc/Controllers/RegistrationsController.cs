﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AngularMvc.ViewModels.Registrations;

namespace AngularMvc.Controllers
{
    public class RegistrationsController : Controller
    {
        private IRegistrationViewModel _registrationViewModel;

        public RegistrationsController()
        {
            _registrationViewModel = new RegistrationViewModel();
        }

        public ActionResult Index()
        {
            return View("Index", "", _registrationViewModel.GetSerializedData());
        }
    }
}
