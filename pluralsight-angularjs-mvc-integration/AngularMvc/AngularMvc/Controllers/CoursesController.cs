﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AngularMvc.ViewModels.Registrations;
using System.Web.Mvc;

namespace AngularMvc.Controllers
{
    public class CoursesController : Controller
    {
        private IRegistrationViewModel _registrationViewModel;

        public CoursesController()
        {
            _registrationViewModel = new RegistrationViewModel();
        }

        public ActionResult Index()
        {
            return View("Index", "", _registrationViewModel.GetSerializedCourseModel());
        }
    }
}