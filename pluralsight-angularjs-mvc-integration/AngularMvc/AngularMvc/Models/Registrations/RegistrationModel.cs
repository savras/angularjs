﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AngularMvc.Models.Registrations
{
    public class RegistrationModel : IRegistrationModel
    {
        public string Course { get; set; }
        public string Instructor{ get; set; }
    }
}