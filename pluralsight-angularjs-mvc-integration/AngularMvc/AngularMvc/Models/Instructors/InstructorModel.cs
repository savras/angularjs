﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AngularMvc.Models.Instructors
{
    public class InstructorModel : IInstructorModel
    {
        public string Name { get; set; }
        public string RoomNumber { get; set; }
        public string Email { get; set; }
    }
}