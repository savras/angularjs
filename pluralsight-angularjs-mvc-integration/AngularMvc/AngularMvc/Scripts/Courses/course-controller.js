﻿//registrationModule.controller("CoursesController", function ($scope, bootstrappedData) {
//    $scope.courses = bootstrappedData.courses;
//});


(function () {
    var registrationModule = angular.module("registrationModule"); // Get reference to registrationModule

    var CoursesController = function ($scope, bootstrappedData) {
        $scope.courses = bootstrappedData.courses;
    };

    registrationModule.controller("CoursesController", CoursesController); // Register new controller
} ());