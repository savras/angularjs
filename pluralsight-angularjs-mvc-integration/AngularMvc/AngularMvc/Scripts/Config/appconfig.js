﻿(function () {
    var app = angular.module("registrationModule", ["ngRoute"]);    
    app.config(function($routeProvider) {
        $routeProvider
          .when("/Courses", {
              templateUrl: "courses.html",
              controller: "CoursesController"
          })
          .when("/Instructors", {
              templateUrl: "instructors.html",
              controller: "InstructorsController"
          })
          .otherwise({
            redirectTo: "/"
          });
          //$locationProvider.html5Mode(true);
  });
} ());