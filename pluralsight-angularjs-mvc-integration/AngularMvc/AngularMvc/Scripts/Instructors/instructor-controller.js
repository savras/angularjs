﻿//registrationModule.controller("InstructorsController", function ($scope, bootstrappedInstructors) {
//    $scope.instructors = bootstrappedInstructors.instructors;
//});


(function () {
    var registrationModule = angular.module("registrationModule"); // Get reference to registrationModule

    var InstructorsController = function ($scope, bootstrappedData) {
        $scope.instructors = bootstrappedData.instructors;
    };

    registrationModule.controller("InstructorsController", InstructorsController); // Register new controller
} ());