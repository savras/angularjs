﻿//
//  The url with "/" prefix is relative to the domain, without the "/" prefix it 
//  will be relative to the main ("index.html") page or base url (if you use location in the html5 mode).
//  e.g. /courses will be localhost/courses
//  e.g. courses will be localhost/registration/courses

//var registrationModule = angular.module("registrationModule", []);
//var registrationModule = angular.module("registrationModule", ["ngRoute"]);
//registrationModule.config(function ($routeProvider) {//, $locationProvider) {
//        $routeProvider
//                .when("Courses", {
//                    templateUrl: "Templates/courses.html",
//                    controller: "CoursesController"
//                })
//                .when("/registration/Courses", {
//                    templateUrl: "Views/Templates/courses.html",
//                    controller: "CoursesController"
//                })
//                .when("/angularmvc/Registrations/Instructors", {
//                    templateUrl: "/Views/Templates/instructors.html",
//                    controller: "InstructorsController"
//                })
//                .otherwise({
//                    redirectTo: "/main"
//                });
//        $locationProvider.html5Mode(true);
//});