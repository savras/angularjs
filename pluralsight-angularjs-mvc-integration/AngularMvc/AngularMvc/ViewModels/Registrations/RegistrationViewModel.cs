﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AngularMvc.Models.Registrations;
using AngularMvc.Models.Courses;
using AngularMvc.Models.Instructors;
using AngularMvc.ViewModels.Courses;
using AngularMvc.ViewModels.Instructors;

namespace AngularMvc.ViewModels.Registrations
{
    public class RegistrationViewModel : IRegistrationViewModel
    {
        private ICourseViewModel _courseViewModel { get; set; }
        private IInstructorViewModel _instructorViewModel { get; set; }
        private SerializedData _serializedData { get; set; }

        public RegistrationViewModel()
        {
            _courseViewModel = new CourseViewModel();
            _instructorViewModel = new InstructorViewModel();
            _serializedData = new SerializedData();
        }

        public string GetSerializedCourseModel()
        {
            return _courseViewModel.GetSerializedCourses();
        }

        public string GetSerializedInstructorModel()
        {
            return _instructorViewModel.GetSerializedInstructors();
        }

        public SerializedData GetSerializedData()
        {
            _serializedData.Courses = GetSerializedCourseModel();
            _serializedData.Instructors = GetSerializedInstructorModel();

            return _serializedData;
        }
    }

    public class SerializedData
    {
        public string Courses { get; set; }
        public string Instructors { get; set; }
    }
}