﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AngularMvc.ViewModels.Registrations
{
    public interface IRegistrationViewModel
    {
        string GetSerializedCourseModel();
        string GetSerializedInstructorModel();
        SerializedData GetSerializedData();
    }
}