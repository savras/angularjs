﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AngularMvc.ViewModels.Courses
{
    public interface ICourseViewModel
    {
        string GetSerializedCourses();
    }
}