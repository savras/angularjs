﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AngularMvc.Models.Courses;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace AngularMvc.ViewModels.Courses
{
    public class CourseViewModel : ICourseViewModel
    {
        public string GetSerializedCourses()
        {
            var courseModels = new[]
                              {
                                  new CourseModel { Instructor = "Care of Magical Creatures", Name = "Rubeus Hagrid", Number = "CREA101" },
                                  new CourseModel { Instructor = "Defence Against the Dark Arts", Name = "Severus Snape", Number = "DARK502" },
                                  new CourseModel { Instructor = "Transfiguration", Name = "Minerva McGonagall", Number = "TRAN201" }
                              };

            var settings = new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver() };
            var serializeCourses = JsonConvert.SerializeObject(courseModels, Formatting.None, settings);

            return serializeCourses;
        }
    }
}