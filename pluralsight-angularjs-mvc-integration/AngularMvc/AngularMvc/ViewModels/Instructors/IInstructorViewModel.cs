﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AngularMvc.ViewModels.Instructors
{
    public interface IInstructorViewModel
    {
        string GetSerializedInstructors();
    }
}