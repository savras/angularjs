﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AngularMvc.Models.Instructors;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace AngularMvc.ViewModels.Instructors
{
    public class InstructorViewModel : IInstructorViewModel
    {
        public string GetSerializedInstructors()
        {
            var instructorModel = new[] 
                    {   
                        new InstructorModel { Name = "Rubeus Hagrid", Email = "rubeus.hagrid@hogwarts.com", RoomNumber = "1001"},
                        new InstructorModel { Name = "Severus Snape", Email = "severus.snape@hogwarts.com", RoomNumber = "324"},
                        new InstructorModel { Name = "Minerva McGonagall", Email = "minerva.mcgonagall@hogwarts.com", RoomNumber = "522"},
                    };

            var settings = new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver() };
            var serializeCourses = JsonConvert.SerializeObject(instructorModel, Formatting.None, settings);

            return serializeCourses;
        }
    }
}