(function() {
	var app = angular.module("githubViewer");	// Get reference to githubViewer
	
	var RepositoryController = function($scope, github, $routeParams) {
		
		var repositoryname = $routeParams.repositoryname;
		var username = $routeParams.username;
		
		var onRepository = function(data) {
			$scope.repository = data;
		};
		
		var onError(reason) {
			$scope.error = reason;
		};	

		github.getRepositoryDetails(username, repositoryname)
			  .then(onRepository, onError);
	};	
	
	app.controller("CollaboratorController", CollaboratorController);	// Register new controller	
}());