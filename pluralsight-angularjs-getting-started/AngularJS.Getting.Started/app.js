(function() {
  var app = angular.module("githubViewer", ["ngRoute"]);

  app.config(function($routeProvider) {
    $routeProvider
      .when("/main", {
        templateUrl: "main.html",
        controller: "MainController"
      })
      .when("/user/:username", {
        templateUrl: "user.html",
        controller: "UserController"
      })
      .when("/collaborators/:repository", {
        templateUrl: "collaborator.html",
        controller: "CollaboratorController"
      })
	  .when("repository/:username/:repositoryname", {
		templateUrl: "repository.html",
		controller: "RepositoryController"
	  }
      .otherwise({
        redirectTo: "/main"
      });
  });
}());