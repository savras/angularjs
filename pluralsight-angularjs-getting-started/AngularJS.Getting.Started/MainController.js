(function() {
  var app = angular.module("githubViewer");

  var MainController = function($scope, $interval, $location) {

    // var onUserComplete = function(response) {
    //var onUserComplete = function(data) {
    // $scope.user = response.data;
    //$scope.user = data;
    // $http.get($scope.user.repos_url)
    //github.get($scope.user.repos_url)
    //.then(onRepos, onError);
    //};

    //var onRepos = function(data) {
    //$scope.repos = data;
    //$location.hash("userDetails");
    //$anchorScroll();
    //}

    //var onError = function(reason) {
    //$scope.error = "Could not fetch data";
    //};

    var decrementCountdown = function() {
      $scope.countdown -= 1;
      if ($scope.countdown < 1) {
        $scope.search($scope.username);
      }
    }

    var countdownInterval = null;
    var startCountdown = function() {
      countdownInterval = $interval(decrementCountdown, 1000, $scope.countdown);
    }

    $scope.search = function(username) {
      //$log.info("Searching for " + username);
      //$http.get("https://api.github.com/users/" + username)
//        .then(onUserComplete, onError);
      if (countdownInterval) {
        $interval.cancel(countdownInterval);
        $scope.countdown = null;
      }
      $location.path("/user/" + username);
    }

    $scope.username = "angular";
    //$scope.message = "GitHub Viewer";
    //$scope.repoSorter = "-stargazer_count";
    $scope.countdown = 5;
    startCountdown();
  }; // end MainController

  app.controller("MainController", MainController);

}());