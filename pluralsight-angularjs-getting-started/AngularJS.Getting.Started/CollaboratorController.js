(function() {
  var app = angular.module("githubViewer");	// Get reference to githubViewer

  var CollaboratorController = function($scope, github, $routeParams) {
    var onCollaboratorsComplete = function(data) {
      $scope.collaborators = data;
    }

    var onError = function(reason) {
      $scope.error = "Could not fetch data.";
    };

    $scope.repository = $routeParams.repository;
    github.getCollaborators($scope.repository)
      .then(onCollaboratorsComplete, onError);
  };

  app.controller("CollaboratorController", CollaboratorController);	// Register new controller
}());