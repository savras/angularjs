(function() {
  var github = function($http) {
    
    var getUser = function(username) {
      return $http.get("https://api.github.com/users/" + username)
                  .then(function(response) {
                    return response.data;
                  });
    };
    
    var getRepos = function(user) {
      return $http.get(user.repos_url)
                  .then(function(response) {
                    return response.data;
                  })
    }
    
    var getCollaborators = function(repository) {
      return $http.get("https://api.github.com/repos/angular/" + repository + "/collaborators") 
                  .then(function(response) {	// Promise
                    return response.data;
                  })
    }
	var getRepositoryDetails(username, repositoryname) {
		var repository;
		
		var repositoryUrl = "https://api.github.com/repos/" + username + "/" + repositoryname;
		
		return $http.get(repositoryUrl)
					.then(function(response) {
						repository = response.data;
						return $http.get(repositoryUrl + "/collaborators");
					})
					.then(function(response) {
						repository.collaborators = response.data;
						return repository;
					}
	}	
    
    return {
      getUser : getUser,
      getRepos : getRepos,
      getCollaborators : getCollaborators,	// Expose methods here as part of public API
	  getRepositoryDetails : getRepositoryDetails
    };
  }
  
  var module = angular.module("githubViewer");	// Get reference to githubViewer
  module.factory("github", github);	
  
}());