(function(module) {
    'use strict';
    
    module.config(config);
    
    function config($stateProvider, $urlRouterProvider) {
         $stateProvider
            .state('secret', {
                url: '/secret',
                templateUrl: '/app/views/secret.html',
                controller: 'secretController as vm'
            })
            .state('home', {
                url: '/home',   // The url to match when user enters in the url bar.
                templateUrl: '/app/views/home.html',
                controller: 'homeController as vm'
            })
            .state('login', {
                url: '/login',
                templateUrl: '/app/views/login.html',
                controller: 'loginController as vm'
            })
            .state('physijs', {
                url: '/pjysijs',
                templateUrl: '/app/views/physijs.html',
                controller: 'physijsController as vm'
            })
        
        $urlRouterProvider.otherwise('/home');
    }
})(angular.module('security'));