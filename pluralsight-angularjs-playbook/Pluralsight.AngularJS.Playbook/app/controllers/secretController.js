(function(module){
    'use strict';
    
    module.controller('secretController', secretController);
    
    function secretController(secretService) {
        var vm = this;
        vm.title = 'The secret recipe is...';
        
        secretService.getOrders().then(onGetOrders, onError);
        
        function onGetOrders(orders) {
            var orders = orders;
        }
        
        function onError(error) {
        }        
    }
})(angular.module('security'));