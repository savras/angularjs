(function(module){
    'use strict';
    
    module.controller('ratingController', ratingController);
    
    function ratingController($scope) {
        // Same isolated scope that the linking function in the rating directive gets.        
        
        // Controller for a directive exposes API to the directive or other directives
        this.initialize = function(min, max) {  // This will be called in the declarative-rating controller itself using 'this'.
            $scope.stars = new Array(max - min + 1)
            $scope.preview = -1;
        };
        
        
        // Setup model for the view
        $scope.click = function(index) {
            $scope.value = index + 1;            
        };
        
        
        $scope.mouseover = function(index){
            $scope.preview = index;
        };
        
        $scope.mouseout = function(){
            $scope.preview = -1;
        };
        
        // ngStyles work by returning a hash and Angular will inspect every property in the object and if it truthy, Angular will take the property 
        // name and add it to the class attribute.
        $scope.styles = function(index) {
            return {
                'glyphicon': true,
                'glyphicon-star': index < $scope.value // same scope in the linking function in the directive. Its an isolated scope an expression that the outside view has given us.
            }
        };
    }
    
})(angular.module('common'));