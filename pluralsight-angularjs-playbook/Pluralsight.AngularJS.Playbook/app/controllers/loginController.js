(function(module){
    'use strict';
    
    module.controller('loginController', loginController);
    
    function loginController(oauth, currentUser, loginRedirect, alerting) {
        var model = this;
        
        model.username = 'a';
        model.password = 'b';
        
        model.user = currentUser.profile;
        
        model.login = login;
        
        model.addAlerts = addAlerts;
        
        function addAlerts() {
            alerting.addWarning('warning message test');
        }
        
        function login(form) {
            if(form.$valid) {
                oauth.login(model.username, model.password)
					.then(onLogin)
                    .catch(onError)
                    
                //(alerting.errorHandler("Could not login"));                
                form.$setUntouched();
            }
        }
        
		function onLogin() {
			loginRedirect.redirectPostLogin();
		}
		
        function onError(response) {
            alert('Error');
        }
    }
    
})(angular.module('security'));