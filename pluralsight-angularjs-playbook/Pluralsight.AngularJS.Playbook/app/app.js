(function() {
    'use strict';
    
    angular.module('security', ['common', 'physics', 'ngResource', 'ui.router']);
    angular.module('common', []);
    angular.module('physics', []);
    
})();