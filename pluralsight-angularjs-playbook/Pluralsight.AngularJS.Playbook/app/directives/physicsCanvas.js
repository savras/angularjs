(function(module) {
    
    module.directive('physicsCanvas', physijsCanvas);
    
    function physijsCanvas(Physics) {
        
        function preLink() {
            var canvas = element[0].querySelector('canvas');
            var renderer = Physics.renderer('canvas', {
                el: canvas,
                width: scope.width,
                height: scope.height
            });
            
            Physics(function(world) {
                scope.world = world;    // Add world to isolated scope object.
                world.add(canvas);
            });  
        }
        
        return {
            restrict: 'E',
            transclude: true,
            templateUrl: 'app/templates/physicsCanvas.html',
            scope: {
                width: '@', // How is this different from '='? This '@' doesn't expect anyone to change the type of object here other than the one at creation.
                height: '@' // Attribute binding. '=' is a modal binding/ two way binding. Attributes are always strings. Must ParseInt();
            },
            controller: function($scope) {  // Allows sharing of controller. This gives the child (physics-body) access to the parent (physics-canvas).
                // Expose an API from this controller. This adds an 'add' method to the controller instance.
                this.add = function(thing) {
                    $scope.world.add(thing);    // Same world as the one in the isolated scope. This canvas controller represents a 'world'.
                                                // This is also the same as the method exposed in the Physics world object. Client expects an 'add' method available so we 
                                                // give them this in the control as well.
                }
            },
            compile: function() {
                return {
                    pre: preLink
                }
            }
// Default is post-linking function. The child runs first. (physics-body then only physics-canvas).
//            link: function(scope, element)  
//            {
//                var canvas = element[0].querySelector('canvas');
//                var renderer = Physics.renderer('canvas', {
//                    el: canvas,
//                    width: scope.width,
//                    height: scope.height
//                });
//                
//                Physics(function(world) {
//                    world.add(canvas);
//                });
//            }
        };
    }
})(angular.module('physics'));