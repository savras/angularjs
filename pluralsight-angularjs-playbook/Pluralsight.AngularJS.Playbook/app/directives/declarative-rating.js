(function(module){
    'use strict';
    
    module.directive('wapolRating', wapolRating);
    
    function wapolRating() {
        return {
            scope: {},
            require: 'wapolRating'  // or require controllers.
            templateUrl: '/app/templates/rating.html',
            controller: 'ratingController',
            link: function(scope, element, attributes, controller){  // Should be used to link scope and DOM, then let the controller take over.                
               
                var min = parseInt(attributes.min || '1');
                var max = parseInt(attributes.max || '10');
                
                controller.initialize(min, max);  // Requiring 'wapolRating' directive will cause Angular to dig up the controller for and inject it as 'controller' parameter after attributes.
            
                // Removed watch and use binding expression in template
                // Removed apply and use ngClick in template
            }
        };
    }
    
})(angular.module('common'));