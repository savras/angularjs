(function(module){
    'use strict';
    
    module.directive('wapolRating', wapolRating);
    
    function wapolRating() {
        return {
            scope: {    // Isolated scope
                value: "="  // Two way binding of a value (property of the same name in the HTML). This will keep the value in HTML in sync with this one.
            },
            link: function(scope, element, attributes){
                // Instead of jumping in and appending an element like below, put it in a watch.                
                // element.append("<button>Some button that will show up in the directive</button>")
                
                // Everything provided through attributes will be a string
                // http://stackoverflow.com/questions/6736476/javascript-parseint-return-nan-for-empty-string
                // When not used with boolean values, the logical OR (||) operator returns the first expression (parseInt(s)) if it can be evaluated to true, otherwise it returns the second expression (0). The return value of parseInt('') is NaN. NaN evaluates to false, so num ends up being set to 0. –  Matt Jul 18 '11 at 17:17
                
                // Pro-tip: When using native events or callbacks, wrap any code that modifies the model in a call to $apply
                var min = parseInt(attributes.min || '1');
                var max = parseInt(attributes.max || '10');
                
                scope.$watch("value", function(newValue){
                    
                    // Watch this value in the two way binding and react when the value changes.
                    // Angular will call this when value changes.
                    element.empty();    // Clear previous elements
                    for(var i = 0; i < newValue; i++) {
                        element.append('<button class="button btn-default btn-xs"><span class="glyphicon glyphicon-star">STAR</span></button>')
                    }                        
                })
                
                // When user clicks on my element or child of this element
                element.on('click', function() {
                    scope.$apply(function() {
                        // Whenever you are changing a Model value but UI isn't updating, usually means you are changing Model value
                        // behind Angular's back. E.g. Wiring up to a native DOM event or executing code that Angular doesn't know about.
                        // so it doesn't run its digest phase to detect changes in the Model.
                        if(scope.value < max){
                            scope.value += 1;   // Two way binding will make Angular keep this value in synch with parent.
                        }
                        else{
                            scope.value = min;
                        }
                    })
                    
                });
            }
        };
    }
    
})(angular.module('common'));