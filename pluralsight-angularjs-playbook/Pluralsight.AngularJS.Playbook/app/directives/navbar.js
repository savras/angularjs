(function(module){
    'use strict';
    
    var navbar = function() {
        return {
            templateUrl: '/app/templates/navbar.html'
        };
    };
    
    module.directive('navbar', navbar);
    
})(angular.module('common'));