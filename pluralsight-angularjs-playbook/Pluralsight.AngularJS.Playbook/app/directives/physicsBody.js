(function(module) {
    module.directive('physicsBody', physicsBody);
    
    function physicsBody(Physics) {
        return {
            restrict: 'E',
            require: '^physicsCanvas',  // ^ means parent controller.
            scope: {
                options: '=',
                body: '=',  // modal binding expressions (two-way). This is bound to an object in the page controller.
                            // The page controller has access to this 'body'
                type: '@', // Don't expect anyone to change the type of the object once it has been created.
            },
            link: function(scope, element, attributes, canvas) {    // canvas is pretty much the controller
                scope.body = Physics.body(scope.type, scope.options);   // attributes on the physics-body element/ control.
                canvas.add(scope.body); // This is the add method exposed by the canvas controller API.
            }
            
        };
    }
    
    physicsBody.$inject = ['Physics'];
})(angular.module('physics'));