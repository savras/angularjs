(function(module){
    'use strict';
    
    module.directive('alerts', alerts);
    
    function alerts(alerting) {
        return {
            restrict: 'AE',
            templateUrl: '/app/templates/alerts.html',
            scope: true,    // New scope object that will inherit from its parent, or {} for an isolated scope.
            controller: function($scope) {    // Injectible scope
              $scope.removeAlert = function(alert) {
                  alerting.removeAlert(alert);
              };
            },
            link: function(scope) {
                scope.currentAlerts = alerting.currentAlerts;
            }
        };
    }
    
    alerts.$inject = ['alerting'];
    
})(angular.module('common'));