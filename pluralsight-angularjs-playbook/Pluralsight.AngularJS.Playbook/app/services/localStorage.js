(function(module){
	'use strict';
	
	module.factory('localStorage', localStorage);
	
	function localStorage($window) {
		var store = $window.localStorage;
		
		return {
			add: add,
			get: get,
			remove: remove
		};
		
		function add(key, value) {
			value = angular.toJson(value);
			store.setItem(key, value);
		}
		
		function get(key) {
			var value = store.getItem(key);
			if(value) {
				value = angular.fromJson(value);
			}
            return value;
		}
		
		function remove(key) {
			store.remove(key);
		}
	}
	
})(angular.module('common'));