(function (module){
    'use strict';
    
    module.factory('oauth', oauth);    
    
    function oauth($http, formEncode, currentUser) {
        var serviceBase = 'http://localhost:52935/';
        
        return {
            login: login
        };
        
        function login(username, password) {
            var config = {
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }

            var data = formEncode({
                username: username,
                password: password,
                grant_type: 'password'
            });

            return $http.post(serviceBase + 'token', data, config)
                .then(onGetToken);
        }
        
        function onGetToken(response) {
            currentUser.setProfile('a', response.data.access_token);
        }
    }
})(angular.module('common'));