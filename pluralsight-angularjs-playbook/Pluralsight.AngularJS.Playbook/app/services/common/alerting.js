(function(module) {
    'use strict';
    
    module.factory('alerting', alerting);
    
    function alerting() {
        var currentAlerts = [];                
    
        var addWarning = function(message) {
            addAlert('warning', message);
        };
        
        var addDanger = function(message) {
            addAlert('danger', message);
        };
        
        var addInfo = function(message) {
            addAlert('info', message);
        };
        
        var addSuccess = function(message) {
            addAlert('success', message);
        };
        
        var addAlert = function(type, message) {
            currentAlerts.push({ type: type, message: message });
        };
        
        var removeAlert = function(alert) {
            for(var i = 0; i < currentAlerts.length; i++) {
                if(currentAlerts[i] === alert) {
                    currentAlerts.splice(i, 1);
                    break;
                }
            }
        };
        
        return {
            addWarning: addWarning,
            addDanger: addDanger,
            addInfo: addInfo,
            addSuccess: addSuccess,
            addAlert: addAlert,
            removeAlert: removeAlert,
            currentAlerts: currentAlerts
        };
    }       
    
})(angular.module('common'));