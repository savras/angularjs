(function(module){
	'use strict';
	
	module.factory('addToken', addToken);
    module.config(function ($httpProvider) {
        $httpProvider.interceptors.push('addToken')
    });
    
	function addToken(currentUser, $q) {		
		return {
			request: request
		}
		
		function request(config) {
            config.headers = config.headers || {};
            
			if(currentUser.profile.loggedIn) {
                console.log(':: Interceptor appending token.');
                
				config.headers.Authorization = "Bearer " + currentUser.profile.token;
			}
			
			return $q.when(config);	// Object wrapped into a promise immediately resolved with that value (config in this case).
		}
	}
	
})(angular.module('common'));