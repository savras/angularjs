(function (module) {
	'use strict';
    
    module.factory('loginRedirect', loginRedirect);
    module.config(function ($httpProvider) {
        $httpProvider.interceptors.push('loginRedirect')
    });
    
	function loginRedirect($q, $location) {
        var lastPath = '/';
        
		return {
			responseError: responseError,
			redirectPostLogin: redirectPostLogin
		}
		
		function responseError(response) {                
            if (response.status === 401) {
                console.log(':: Intercepted 401 response. Redirecting to login.');

                lastPath = $location.path();
                $location.path('/login');
            }

            return $q.reject(response);
		}
		
		function redirectPostLogin() {
			$location.path(lastPath);
			lastPath = '/';
		}
	};    
                      
})(angular.module('common'));