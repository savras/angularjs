(function(module) {
    'use strict';
    
    module.factory('currentUser', currentUser);
    
    function currentUser(localStorage) {
		var USERKEY = 'utoken';
		
        var profile =  initialize();
        
        return {
            setProfile: setProfile,
            profile: profile
        };

		function initialize() {
			var user = {			
				username: '',
				token: '',
				get loggedIn() {
					return this.token;
                }
            };
			
			var localUser = localStorage.get(USERKEY);
			if(localUser) {
				user.username = localUser.username;
				user.token = localUser.token;
			}
			return user;
		}
        
        function setProfile(username, token){
            profile.username = username;
            profile.token = token;
			
			localStorage.add(USERKEY, profile);
        }
    }
    
})(angular.module('common'));