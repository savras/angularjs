(function(module){
    'use strict';
    
    module.service('secretService', secretService);
    
    function secretService($resource) {
        var secret = $resource('http://localhost:52935/orders/', {}, {} );
        
        return {
            getOrders: getOrders
        };
        
        function getOrders() {
            return secret.query().$promise;
        }                               
    }
})(angular.module('security'));